package moe.tto.justbrowser.Tab

import android.app.ListActivity
import android.content.res.TypedArray
import android.os.Bundle
import android.widget.AdapterView

import moe.tto.justbrowser.Adapter.SwitchTabDialogAdapter

class TabListDialog : ListActivity() {

    var names: Array<String>? = null
    private val imgs: TypedArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = intent.getBundleExtra("bundle")
        if(bundle != null) {
            val inList = bundle.getSerializable("tab_array")
            if (inList != null) {
                val list = inList as List<TabItem>
                val adapter = SwitchTabDialogAdapter(this, list)
                listAdapter = adapter
                listView.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id -> }
            }
        }
    }

    private fun populateList() {

    }
}