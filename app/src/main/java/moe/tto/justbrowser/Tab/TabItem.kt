package moe.tto.justbrowser.Tab

import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable


class TabItem(var url:String, var title:String, var screenshot: Bitmap): Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(Bitmap::class.java.classLoader)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(url)
        parcel.writeString(title)
        parcel.writeParcelable(screenshot, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TabItem> {
        override fun createFromParcel(parcel: Parcel): TabItem {
            return TabItem(parcel)
        }

        override fun newArray(size: Int): Array<TabItem?> {
            return arrayOfNulls(size)
        }
    }

}