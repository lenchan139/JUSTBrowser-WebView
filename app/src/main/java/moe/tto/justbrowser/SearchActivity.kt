package moe.tto.justbrowser

import android.content.Context
import android.content.Intent
import android.database.sqlite.SQLiteException
import android.media.Image
import android.os.Build
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.EditText
import android.widget.ImageView
import android.widget.ListView

import moe.tto.justbrowser.CustomScript.CustomScriptItem
import moe.tto.justbrowser.History.HistroySQLiteController

import java.util.ArrayList
import kotlinx.android.synthetic.main.activity_search.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import moe.tto.justbrowser.Adapter.SearchListItemAdapter
import moe.tto.justbrowser.Const.CommonStrings
import moe.tto.justbrowser.Items.SearchListItem

import java.net.URL
import java.nio.charset.Charset


class SearchActivity : AppCompatActivity() {
    private var histroySQLiteController: HistroySQLiteController? = null
    internal var historyList = ArrayList<CustomScriptItem>()
    internal lateinit var hList: ListView
    lateinit var searchViewEditText : EditText
    lateinit var searchViewIcon : ImageView
    private val commonStrings = CommonStrings(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        histroySQLiteController = HistroySQLiteController(this)
        hList = findViewById(R.id.list)
        fab.setOnClickListener {
            hideKeyboard()
            finish()
        }
        searchViewEditText = searchView.findViewById(R.id.search_src_text)
        searchViewIcon = searchView.findViewById(R.id.search_mag_icon)
        val window = this.window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)

        searchViewIcon.visibility = View.GONE
        searchViewIcon.setImageDrawable(null)
        searchViewEditText.requestFocus()
        searchView.setIconifiedByDefault(false)
        searchViewEditText.setText(intent.getStringExtra("para"))
        searchViewEditText.selectAll()

        val onQueryTextList = object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                onEnter()
                return false
            }

            override fun onQueryTextChange(query: String?): Boolean {
                return false
            }

        }
        searchView.setOnQueryTextListener(onQueryTextList)

        //update
        searchViewEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                updateListView(s.toString())
                //editText.setSelection(editText.text.length)
            }
        })
        initSearchView()
    }

    override fun onPostResume() {
        super.onPostResume()
    }

    fun initSearchView(){
    }
    override fun onPause() {
        finish()
        super.onPause()
    }

    private fun updateListView(query:String) {

        historyList.clear()
        var showlist = ArrayList<SearchListItem>()
        doAsync {
            try{
                showlist = downloadInstantSuggest(query, showlist)
            }catch (e:JSONException){
                e.printStackTrace()
            }
            try {
                showlist = getHistoryFromKeyword(query, showlist)
            }catch (e:SQLiteException){
                e.printStackTrace()
            }
            uiThread{
                updateListViewUI(showlist)
            }
        }

    }

    fun downloadInstantSuggest(keyword : String?, showlist: ArrayList<SearchListItem>):ArrayList<SearchListItem>{
        if(keyword != null && keyword.isNotEmpty()){
            Log.v("DDG suggests: ", commonStrings.getInstantSuggestApiUrl(keyword))
            val content = URL(commonStrings.getInstantSuggestApiUrl(keyword)).readText(Charset.defaultCharset())
            val json = JSONArray(content)
            if(json[0] == keyword){
                val suggests = json[1] as JSONArray
                val strSuggestsArray = Array<String>(suggests.length(), {""})
                for(i in 0..suggests.length()-1){
                    if(i>2){
                        break
                    }
                    strSuggestsArray[i] = suggests[i] as String
                    val title = "DuckDuckGo Suggests: " + strSuggestsArray[i]
                    showlist.add(SearchListItem(getDrawable(R.drawable.search_item_duckduckgo), title, strSuggestsArray[i]))

                }
                return showlist
            }else{
                throw JSONException("Invalid JSON")
            }
        }
        return showlist
    }

    private fun getHistoryFromKeyword(keyword: String?, showlist: ArrayList<SearchListItem>):ArrayList<SearchListItem>{
        historyList = histroySQLiteController!!.getHistoryBySearchUrl(keyword)


        for (i in historyList.indices) {
            if(historyList[i].title != null && historyList[i].url != null){
                showlist.add(SearchListItem(null, historyList[i].title!!, historyList[i].url!!))
            }
        }
        return showlist
    }
    private fun updateListViewUI(showlist:ArrayList<SearchListItem>){
        val adapter = SearchListItemAdapter(this, showlist)
        hList.adapter = adapter
        hList.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            searchViewEditText.setText(showlist[position].url)
            onEnter()
        }
        hList.deferNotifyDataSetChanged()
    }

    private fun onEnter() {
        val intent = Intent(this@SearchActivity, BrowseActivity::class.java)
        intent.putExtra("InURL", searchViewEditText.text.toString())
        startActivity(intent)
        finish()
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

}



