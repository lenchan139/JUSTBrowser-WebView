package moe.tto.justbrowser.Utils

import android.graphics.Bitmap
import android.view.View

object ScreenshotUtils {

    fun takescreenshot(v: View): Bitmap {
        v.isDrawingCacheEnabled = true
        v.buildDrawingCache(true)
        val b = Bitmap.createBitmap(v.drawingCache)
        v.isDrawingCacheEnabled = false
        return b
    }

    fun takescreenshotOfRootView(v: View): Bitmap {
        return takescreenshot(v.rootView)
    }
    fun cropTo100pxSqure(scrBmp:Bitmap):Bitmap{
        return Bitmap.createScaledBitmap(cropToSqure(scrBmp),100,100,true)
    }
    fun cropToSqure(srcBmp:Bitmap):Bitmap{
        val dstBmp : Bitmap
        if (srcBmp.getWidth() >= srcBmp.getHeight()){

            dstBmp = Bitmap.createBitmap(
                    srcBmp,
                    srcBmp.getWidth()/2 - srcBmp.getHeight()/2,
                    0,
                    srcBmp.getHeight(),
                    srcBmp.getHeight()
            )

        }else{

            dstBmp = Bitmap.createBitmap(
                    srcBmp,
                    0,
                    srcBmp.getHeight()/2 - srcBmp.getWidth()/2,
                    srcBmp.getWidth(),
                    srcBmp.getWidth()
            )
        }
        return dstBmp
    }
}
