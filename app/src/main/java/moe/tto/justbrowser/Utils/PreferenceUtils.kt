package moe.tto.justbrowser.Utils

import android.content.Context
import moe.tto.justbrowser.R
import org.jetbrains.anko.defaultSharedPreferences

class PreferenceUtils(val context: Context){
    private val preferenceManager = context.defaultSharedPreferences
    val share_body_max_char_limit : Int
        get() {
            val result:String = preferenceManager.getString(ridToString(R.string.pref_key_share_body_max_char_limit), null ) ?: "100"
            return result.toInt()
        }



    fun getNCLoginUsername():String{
        return preferenceManager.getString(ridToString(R.string.pref_key_ncbookmark_login_username), null) ?: ""
    }
    fun getNCLoginPassword():String{
        return preferenceManager.getString(ridToString(R.string.pref_key_ncbookmark_login_password), null) ?: ""
    }
    fun getNCLoginUrl():String{
        return preferenceManager.getString(ridToString(R.string.pref_key_ncbookmark_login_url), null) ?: ""
    }
    fun setNCLoginUrl(newValue:String){
        preferenceManager.edit().putString(ridToString(R.string.pref_key_ncbookmark_login_url), newValue).apply()
    }

    fun setNCLoginUsername(newValue:String){
        preferenceManager.edit().putString(ridToString(R.string.pref_key_ncbookmark_login_username), newValue).apply()
    }

    fun setNCLoginPassword(newValue:String){
        preferenceManager.edit().putString(ridToString(R.string.pref_key_ncbookmark_login_password), newValue).apply()
    }
    fun cleanNCLoginUsernamePassswordUrl(){
        preferenceManager
                .edit()
                .remove(ridToString(R.string.pref_key_ncbookmark_login_url))
                .remove(ridToString(R.string.pref_key_ncbookmark_login_username))
                .remove(ridToString(R.string.pref_key_ncbookmark_login_password))
                .apply()
    }
    fun ridToString(rid:Int):String{
        return context.getString(rid)
    }
}