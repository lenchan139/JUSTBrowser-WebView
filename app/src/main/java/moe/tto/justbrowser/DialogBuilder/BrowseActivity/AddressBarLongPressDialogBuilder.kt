package moe.tto.justbrowser.DialogBuilder.BrowseActivity

import android.content.DialogInterface
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import moe.tto.justbrowser.BrowseActivity
import moe.tto.justbrowser.Utils.ContentUtils

class AddressBarLongPressDialogBuilder{
    companion object {
        fun create(activity:BrowseActivity, title:String): AlertDialog {
            val contentUtils = ContentUtils(activity)
            val menuOptions = arrayOf("Parse & Go", "Copy Link")
            val adapter = ArrayAdapter(activity, android.R.layout.simple_list_item_1, menuOptions)
            val dialog = AlertDialog.Builder(activity)
                    .setTitle(title)
                    .setAdapter(adapter, DialogInterface.OnClickListener { dialog, which ->
                        when(which){
                            0 -> activity.loadUrlToFragment(contentUtils.copyFromClipboard(null))
                            1 -> contentUtils.copyToClipboard(activity.getCurrentFragment().getWebUrl())
                        }
                    }).create()
            return dialog


        }
    }
}