package moe.tto.justbrowser.Fragment

import android.app.Activity
import android.app.DownloadManager
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.AudioAttributes
import android.media.AudioFocusRequest
import android.media.AudioManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import android.widget.Toast
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.evernote.android.state.State
import icepick.Icepick
import icepick.Icicle
import kotlinx.android.synthetic.main.fragment_browse.view.*
import moe.tto.justbrowser.BrowseActivity
import moe.tto.justbrowser.Const.CommonStrings
import moe.tto.justbrowser.Utils.FileUtils
import moe.tto.justbrowser.Utils.ScreenshotUtils
import moe.tto.justbrowser.R
import moe.tto.justbrowser.WebViewClass.WebChromeClientOverride
import moe.tto.justbrowser.WebViewClass.WebViewClientOverride
import moe.tto.justbrowser.WebViewClass.WebViewOverride
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File


/**
 * A placeholder fragment containing a simple view.
 */

class BrowseFragment : Fragment() {
     var mUploadMessage: ValueCallback<Array<Uri>>? = null
     val FILECHOOSER_RESULTCODE = 859
    @State lateinit var rootView  : View
    @State lateinit var settings : SharedPreferences
    @State lateinit var commonStrings : CommonStrings
    @State lateinit var activity : BrowseActivity
    @State var url = ""
    @State var title = ""
    @State var section_number: Int = -1
    @State var webViewState : Bundle? = null

    @Icicle var jsConsoleLogs = ArrayList<String>()
    var isInit = false
    @Icicle var lastScreenshot : Bitmap = Bitmap.createBitmap(100,100,Bitmap.Config.ARGB_4444)
    @Icicle lateinit var webView : WebViewOverride
    private var lastExternalUrl = ""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.v("fragmentLifeCycle",this.toString() + "onCreateView")
        rootView = inflater.inflate(R.layout.fragment_browse, container, false)
        activity = getActivity() as BrowseActivity
        commonStrings = CommonStrings(activity)
        settings = PreferenceManager.getDefaultSharedPreferences(activity)
        if(savedInstanceState != null){
            Icepick.restoreInstanceState(this,savedInstanceState)
        }else{

            Log.v("TaskIdOfFragment",this.toString())
        }
        rootView.refresh_layout.setOnRefreshListener {
            webView.reload()
        }
        rootView.refresh_layout.setOnChildScrollUpCallback(object : SwipeRefreshLayout.OnChildScrollUpCallback {
            override fun canChildScrollUp(parent: SwipeRefreshLayout, child: View?): Boolean {
                Log.v("ScrollTopEqualZero", "${webView.scrollX}|${webView.scrollY}|${webView.isAllElementsOnTop}")
                return webView.scrollY > 0 || !webView.isAllElementsOnTop
            }

        })
        webView = initWebView(rootView.webView as WebViewOverride)
        initFindContents()
        return rootView
    }

    override fun onSaveInstanceState(outState: Bundle) {
        try {
            Log.v("fragmentLifeCycle", this.toString() + "onSaveInstanceState")
            super.onSaveInstanceState(outState)
            webView.saveState(outState)
            Icepick.saveInstanceState(this, outState)
        }catch (e:UninitializedPropertyAccessException){
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)
        Log.i("here123","before")
        if (requestCode == FILECHOOSER_RESULTCODE) {
            //Log.i("here123","after")
            if (null == mUploadMessage)
            {
                //mUploadMessage!!.onReceiveValue(null)
                return
            }
            val result = if (data == null || resultCode != Activity.RESULT_OK) null else data.data
            if (result == null) {
                mUploadMessage!!.onReceiveValue(null)
                mUploadMessage = null
                return
            }

            Log.i("UPFILE", "onActivityResult" + result.toString());
            val path = FileUtils.getPath(activity, result)
            if (TextUtils.isEmpty(path)) {
                mUploadMessage!!.onReceiveValue(null)
                mUploadMessage = null
                return
            }
            val uri = Uri.fromFile(File(path!!))
            Log.i("UPFILE", "onActivityResult after parser uri:" + uri.toString());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mUploadMessage!!.onReceiveValue(arrayOf(uri))
            } else {
                mUploadMessage!!.onReceiveValue(null)
            }
            mUploadMessage = null
        }
    }
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        Log.v("fragmentLifeCycle",this.toString() + "onViewStateRestored")
        super.onViewStateRestored(savedInstanceState)
    }

    override fun onPause() {
        Log.v("fragmentLifeCycle",this.toString() + "onPause")
        super.onPause()
        webViewState = Bundle()
        webView.saveState(webViewState)
        webView.onPause()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        addToBrowseFragmentArrayList()
    }

    override fun onAttach(activity: Activity) {
        Log.v("fragmentLifeCycle",this.toString() + "onAttach")
        super.onAttach(activity)
        addToBrowseFragmentArrayList()
    }
    fun addToBrowseFragmentArrayList(){
        if(!isInit){
            val a = getActivity() as BrowseActivity
            a.arrBrowseFragment.add(this)
            isInit = true
        }
    }
    fun removeFromBrowseFragmentArrayList(){
        val a = getActivity() as BrowseActivity
        a.arrBrowseFragment.remove(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.v("fragmentLifeCycle",this.toString() + "onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        val homeUrl = settings.getString(commonStrings.TAG_pref_home(), commonStrings.URL_DDG())
        val lastUrl = settings.getString(commonStrings.getLastUrlPrefTag(), homeUrl)
        if (webViewState != null) {
            webView.restoreState(webViewState);
        } else if (savedInstanceState != null) {
            webView.restoreState(savedInstanceState)
        } else if(activity.incomeUrlOnStart != null){
            loadUrl(activity.incomeUrlOnStart!!)
            activity.incomeUrlOnStart = null
        }else{
            loadUrl(lastUrl)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        Log.v("userVisiHint", isVisibleToUser.toString())
        if(isInit) {
            try {
                if (isVisibleToUser) {
                    onResume()
                } else {
                    onPause()
                }
            }catch(e:UninitializedPropertyAccessException){
                isInit = false
            }
        }
    }

    override fun onResume() {
        Log.v("fragmentLifeCycle",this.toString() + "onResume")
        super.onResume()
        webView.onResume()
        reloadWebviewBuildinPreference()
    }

    override fun onStop() {
        Log.v("fragmentLifeCycle",this.toString() + "onStop")
        super.onStop()
    }

    override fun onDestroy() {
        Log.v("fragmentLifeCycle",this.toString() + "onDestroy")
        super.onDestroy()
    }

    override fun onDestroyView() {
        Log.v("fragmentLifeCycle",this.toString() + "onDestroyView")
        super.onDestroyView()
    }

    override fun onDetach() {
        Log.v("fragmentLifeCycle",this.toString() + "onDetach")
        removeFromBrowseFragmentArrayList()
        isInit = false
        super.onDetach()

    }

    fun updateScreenshot(){
        try{
            val bitmap = ScreenshotUtils.takescreenshot(rootView.webView)
            lastScreenshot = ScreenshotUtils.cropTo100pxSqure(bitmap)
        }catch (e:NullPointerException){
            e.printStackTrace()
        }

    }
    fun reloadWebviewBuildinPreference(){
        val settings = activity.settings
        webView.settings.javaScriptEnabled = settings.getBoolean(commonStrings.TAG_pref_enable_javascript(),true)
        webView.settings.domStorageEnabled = settings.getBoolean(commonStrings.TAG_pref_allow_dom_storage(), false)
        webView.settings.displayZoomControls = settings.getBoolean(commonStrings.TAG_pref_enable_buildin_zooming(), true)
        webView.settings.builtInZoomControls = settings.getBoolean(commonStrings.TAG_pref_allow_zooming(), false)
        webView.settings.setAppCacheEnabled(settings.getBoolean(commonStrings.TAG_pref_enable_webview_cache(),false))
        webView.settings.savePassword = settings.getBoolean(commonStrings.TAG_pref_save_password(), false)

        webView.settings.loadsImagesAutomatically = settings.getBoolean(commonStrings.TAG_pref_image_auto_load(), true)


    }
    fun initFindContents(){
        rootView.btnFindBack.setOnClickListener {
            findPrevious()
        }
        rootView.btnFindForward.setOnClickListener {
            findNext()
        }
        rootView.btnFindClose.setOnClickListener {
            endFindContent()
        }
        rootView.tabBarFind.visibility = View.GONE
    }
    fun getWebTitle():String{
        return webView.title
    }
    fun getWebUrl():String{
        return webView.url
    }
    fun startFindContent(keyword:String){
        webView.findAllAsync(keyword)
        rootView.tabBarFind.visibility = View.VISIBLE

    }
    fun endFindContent(){
        rootView.tabBarFind.visibility = View.GONE
        webView.findAllAsync("")
    }
    fun findNext(){
        webView.findNext(true)
    }
    fun findPrevious(){
        webView.findNext(false)
    }
    fun isInBrwse(packageName: String):Boolean{

        val intentFilter = IntentFilter()
        val outFilters =  ArrayList<IntentFilter>()
        val outActivities = ArrayList<ComponentName>()

        val filterIntent = Intent(Intent.ACTION_VIEW,Uri.parse("http://example.com/"))
        val filterTargetedIntents = ArrayList<Intent>()
        val filteredInfo = activity.packageManager.queryIntentActivities(filterIntent, PackageManager.MATCH_ALL)

        for (info in filteredInfo) {
            val targetedShareIntent = Intent(Intent.ACTION_VIEW,Uri.parse(url))
            targetedShareIntent.setPackage(info.activityInfo.packageName)
            filterTargetedIntents.add(targetedShareIntent)
            Log.v("CATS_ ", info.activityInfo.packageName)
            if(packageName == info.activityInfo.packageName){
                return true
            }
        }
        return false
    }
    fun isOpenExternal(url:String):Boolean{
        if(url.startsWith(getString(R.string.prefix_intent_scheme)))
            return true
        val preIntent = Intent(Intent.ACTION_VIEW,Uri.parse(url))
        val targetedShareIntents = ArrayList<Intent>()
        val resInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.packageManager.queryIntentActivities(preIntent, PackageManager.MATCH_ALL)
        } else {
            activity.packageManager.queryIntentActivities(preIntent, PackageManager.MATCH_DEFAULT_ONLY)
        }

        for (resolveInfo in resInfo){
            Log.v("listV",resolveInfo.activityInfo.packageName)
            val packageName = resolveInfo.activityInfo.packageName

            val targetedShareIntent = Intent(Intent.ACTION_VIEW,Uri.parse(url))
            targetedShareIntent.setPackage(packageName)
            if(!packageName.contains(activity.applicationContext.packageName) && !isInBrwse(resolveInfo.activityInfo.packageName)){
                targetedShareIntents.add(targetedShareIntent)
                Log.v("listVTureFalse","True")
            }
        }
        if(targetedShareIntents.size >= 1) {
            return true
        }else{
            return false
        }
    }
    fun tryOpenExternal(url: String) {

        Log.v("runToExternalUrl", url)
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)
            val isAutoOpenExternal = sharedPreferences.getBoolean(commonStrings.TAG_pref_auto_open_external_when_loading(), false)
            if (isOpenExternal(url) && isAutoOpenExternal) {
                if(url.equals(lastExternalUrl)){
                }else{
                    activity.runToExternal(this, url)
                    lastExternalUrl = url
                }
                activity.decideOpenExternalButton(url)
            }else if(isOpenExternal(url)){
                activity.decideOpenExternalButton(url)
            }else{
                activity.decideOpenExternalButton(null)
            }
    }

    fun loadUrl (url:String): Boolean{
        val temp = url.trim { it <= ' ' }
        val webView = webView
        if (temp.startsWith("javascript:")) {

            val oldUrl = webView.url
            webView.loadUrl(temp)
            activity.updateEditTextFromCurrentPage(this, oldUrl)

        } else if (temp.startsWith("https://") || temp.startsWith("http://")) {
            webView.loadUrl(temp)
        } else if (temp.indexOf(":") >= 1) {
            activity.runToExternal(this, temp)
        } else if (!(temp.contains(".") && !temp.contains(" "))) {
            webView.loadUrl(settings.getString(commonStrings.TAG_pref_Search_Engine_Url(),
                    commonStrings.ARRAY_pref_Search_Engine_Default()[0].url).replace("!@keywoard",temp))
        } else {
            webView.loadUrl("http://" + temp)
        }
        return true
    }
    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int): BrowseFragment {
            val fragment = BrowseFragment()
            val args = Bundle()
            fragment.section_number = sectionNumber
            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }

    class JavascriptWebViewInterface(val activity: BrowseActivity){
        @JavascriptInterface
        fun onUrlChange(url:String) {
            Log.d("hydrated", "onUrlChange" + url)
            val fragment = activity.getCurrentFragment()
            doAsync {
                uiThread {
                    if(url.isNotEmpty() && fragment.isInit)
                        fragment.tryOpenExternal(url)
                    activity.updateEditTextFromCurrentPage(fragment, url)
                }
            }

        }
        @JavascriptInterface
        fun onVideoPlay(){
            Log.v("onVideoPlay","true")
            //Toast.makeText(activity, "Play", Toast.LENGTH_SHORT).show();
        }
        @JavascriptInterface
        fun onVideoPause(){
            Log.v("onVideoPause", "true")
            //Toast.makeText(activity, "Pause", Toast.LENGTH_SHORT).show();
        }
    }
    fun getCurrWebView(): WebViewOverride {return webView}
    fun initWebView(webView : WebViewOverride): WebViewOverride {
        webView.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
        settings.edit().putString(commonStrings.TAG_pref_custom_user_agent_default(),webView.settings.userAgentString).apply()
        val default = webView.settings.userAgentString
        webView.settings.userAgentString = settings.getString(commonStrings.TAG_pref_custom_user_agent(),default)
        webView.addJavascriptInterface(JavascriptWebViewInterface(activity),"android")

        webView.setFindListener ( object : WebView.FindListener{
            override fun onFindResultReceived(activeMatchOrdinal: Int, numberOfMatches: Int, isDoneCounting: Boolean) {

            }
        }


        )
        webView.setDownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            val request = DownloadManager.Request(
                    Uri.parse(url))
            val cm = CookieManager.getInstance().getCookie(url)
            MimeTypeMap.getSingleton().getExtensionFromMimeType(mimetype)
            val fileName = URLUtil.guessFileName(url, contentDisposition, mimetype)
            var downloadId : Long = -1

            request.allowScanningByMediaScanner()
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED) //Notify client once download is completed!
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,  fileName)
            request.addRequestHeader("Cookie", cm)
            object : BroadcastReceiver(){

                override fun onReceive(contxt: Context?, intent: Intent?) {
                    val action = intent?.action
                    val manager = activity.getSystemService(AppCompatActivity.DOWNLOAD_SERVICE) as DownloadManager
                    if(DownloadManager.ACTION_DOWNLOAD_COMPLETE == action && (downloadId >= 0) ){
                        val query = DownloadManager.Query()
                        query.setFilterById(downloadId)
                        val c = manager.query(query)
                        if (c.moveToFirst()) {
                            val columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS)
                            if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                                var uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI))
                                uriString = uriString.replace("file:///","")
                                val apkFile = File(uriString)

                                Log.v("onDownloadConplete",uriString)
                                if(apkFile.extension == "apk"){
                                    activity.installApk(activity,apkFile)
                                }
                                Toast.makeText(activity, "download success", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
            }
            //registerReceiver(onComplete, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
            request.setDescription("[Download task from Light Browser]")
            request.allowScanningByMediaScanner()
            val dm = activity.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            downloadId = dm.enqueue(request)
            Log.v("downloadMimeType", mimetype)
            Toast.makeText(activity, "Downloading...", //To notify the Client that the file is being downloaded
                    Toast.LENGTH_LONG).show()
        }
        webView.webChromeClient = WebChromeClientOverride(activity, this)
        webView.webViewClient = WebViewClientOverride(webView, activity, this)
        webView.settings.setUseWideViewPort(true)
        webView.canScrollHorizontally(1)
        webView.settings.loadWithOverviewMode = true
        webView.settings.mediaPlaybackRequiresUserGesture = true
        return webView
    }
    fun updateUrlUseJavascript(view: WebView?){
        view?.loadUrl("javascript:window.android.onUrlChange(window.location.href);")

        //init for video control
        view?.evaluateJavascript("" +
                "var v = document.getElementsByTagName('video');" +
                "for(i in v){ " +
                "console.log('yoyoyo');" +
                "v[i].onplay = function(){window.android.onVideoPlay();};" +
                "v[i].onpause = function(){window.android.onVideoPause();};" +
                "}", null)
    }
}