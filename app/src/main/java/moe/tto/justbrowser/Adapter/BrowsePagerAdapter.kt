package moe.tto.justbrowser.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import moe.tto.justbrowser.Fragment.BrowseFragment

class BrowsePagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {
    val fragmentsList = ArrayList<BrowseFragment>()
    override fun getCount(): Int {
        return fragmentsList.size
    }

    override fun getItem(position: Int): Fragment {
        val newFragment = BrowseFragment();
        fragmentsList.add(newFragment)
        return newFragment
    }
}