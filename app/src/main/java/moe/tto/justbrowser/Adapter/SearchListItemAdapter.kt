package moe.tto.justbrowser.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import moe.tto.justbrowser.Items.SearchListItem
import moe.tto.justbrowser.R

import moe.tto.justbrowser.SearchActivity

class SearchListItemAdapter(var activity:SearchActivity, var items:ArrayList<SearchListItem>):BaseAdapter(){
    val thisInflater = LayoutInflater.from(activity)
    override fun getCount(): Int {
        return items.size
    }

    override fun getItemId(position: Int): Long {
        return items.indexOf(getItem(position)).toLong()
    }

    override fun getItem(position: Int): SearchListItem {
        return items.get(position)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val holder : moe.tto.justbrowser.Adapter.SearchListItemAdapter.ViewHolder?
        var view = convertView
        if(convertView == null){
            view = thisInflater.inflate(R.layout.search_list_item, null)
            holder = moe.tto.justbrowser.Adapter.SearchListItemAdapter.ViewHolder(
                    view.findViewById(R.id.icon),
                    view.findViewById(R.id.title),
                    view.findViewById(R.id.url))
            view.setTag(holder)

        }else{
            holder = view!!.getTag() as moe.tto.justbrowser.Adapter.SearchListItemAdapter.ViewHolder
        }

        val item = getItem(position)
        if(item.icon == null){
            holder.icon.setImageDrawable(activity.getDrawable(R.drawable.search_item_basic))
        }else{
            holder.icon.setImageDrawable(item.icon)
        }
        holder.title.setText(item.title)
        holder.url.setText(item.url)
        return view!!
    }
    private class ViewHolder(val icon:ImageView, val title:TextView, val url:TextView)

}