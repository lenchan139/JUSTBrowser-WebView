package moe.tto.justbrowser.Adapter;

import android.app.Activity;
import android.graphics.Color;
import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import moe.tto.justbrowser.Tab.TabItem;
import moe.tto.justbrowser.R;
;

import java.util.List;

public class SwitchTabDialogAdapter extends ArrayAdapter<TabItem> {

    private SwitchTabDialogAdapterCallback callback;
    private final List<TabItem> list;
    private final Activity context;
    public Integer currentPage;
    public AlertDialog dialog;
    static class ViewHolder {
        protected TextView title;
        protected TextView url;
        protected ImageView screenshot;
    }

    public SwitchTabDialogAdapter(Activity context, List<TabItem> list) {
        super(context, R.layout.switch_tab_item_view, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = null;

        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            view = inflator.inflate(R.layout.switch_tab_item_view, null);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.title = (TextView) view.findViewById(R.id.title);
            viewHolder.url = (TextView) view.findViewById(R.id.url);
            viewHolder.screenshot = (ImageView) view.findViewById(R.id.screenshot);
            view.setTag(viewHolder);
        } else {
            view = convertView;
        }

        ViewHolder holder = (ViewHolder) view.getTag();

        holder.url.setText(list.get(position).getUrl());
        holder.title.setText(list.get(position).getTitle());
        Glide.with(view).load(list.get(position).getScreenshot()).into(holder.screenshot);
        view.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                callback.switchToTab(dialog,position);

            }
        });
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                String titleUrl = String.format("[%s](%s)",list.get(position).getTitle(),list.get(position).getUrl());
                callback.removeToTab(dialog,position,titleUrl);

                return true;
            }
        });
        if(currentPage == position){
            view.setBackground(getContext().getDrawable(R.drawable.bg_key));

        }else{
            view.setBackgroundColor(Color.WHITE);
        }
        return view;
    }
    public void setCallback(SwitchTabDialogAdapterCallback callback){

        this.callback = callback;
    }


    public interface SwitchTabDialogAdapterCallback {

        public void switchToTab(AlertDialog dialog,int position);

        public void removeToTab(AlertDialog dialog,int position, String titleUrl);
    }
}