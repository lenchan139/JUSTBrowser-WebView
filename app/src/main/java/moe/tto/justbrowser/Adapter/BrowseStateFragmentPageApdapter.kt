package moe.tto.justbrowser.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.view.ViewGroup
import moe.tto.justbrowser.Adapter.arraypageradapter.ArrayFragmentPagerAdapter
import moe.tto.justbrowser.Fragment.BrowseFragment

class BrowseStateFragmentPageApdapter(fm: FragmentManager, datas: ArrayList<String>) : ArrayFragmentPagerAdapter<String>(fm, datas) {
    var currentPosition = -1
    var arrFragment = ArrayList<BrowseFragment>()
    val BROWSE_ITEM_KEYSTRING = "BROWSE_ITEM_KEYSTRING"
    override fun getFragment(item: String, position: Int): Fragment {
        currentPosition = 0
        val fragment = BrowseFragment.newInstance(position)
        arrFragment.add(fragment)
        return fragment
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
        if(position<arrFragment.size){
            arrFragment.removeAt(position)
        }
    }

    fun getFragment( position: Int): Fragment {
        currentPosition = 0

        return getFragment(BROWSE_ITEM_KEYSTRING, position)
    }
    fun getFragmentId(position: Int):Int {
        return getFragment(position).id
    }
}
