package moe.tto.justbrowser.Const

class WebElementLongClickMenuOptions{
    val copyUrl = 1
    val shareUrl = 2
    val saveImage = 3
    val viewImage = 4
    val saveVideo = 5
    val videoFullscreen = 6
}