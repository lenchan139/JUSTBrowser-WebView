package moe.tto.justbrowser.Const

import android.content.Context
import moe.tto.justbrowser.R

import java.net.URLEncoder


/**
 * Created by len on 10/15/16.
 */

class CommonStrings constructor(context:Context) {
    var context = context
    fun searchHeader(): String {
        return "https://duckduckgo.com/?q="
    }

    fun mobilizeHeader(): String {
        return "https://googleweblight.com/?lite_url="
    }

    fun homePage(): String {
        return "https://ddg.gg"
    }

    fun TAG_setting(): String {
        return "org.lenchan139.lightbrowser.settings"
    }

    fun TAG_pref_home(): String {
        return "org.lenchan139.lightbrowser.Home"
    }
    fun TAG_pref_enable_adblocker():String{
        return "org.lenchan139.lightbrowser.TAG_pref_adblock_switch"
    }

    fun TAG_pref_adblocker_url():String{
        return "org.lenchan139.lightbrowser.TAG_pref_adblocker_url"
    }
    fun TAG_pref_remove_instead_restart_when_tab_mismatch():String{
        return "org.lenchan139.lightbrowser.TAG_pref_reopen_when_fragments_mismatch"
    }
    fun TAG_pref_fab(): String {
        return "org.lenchan139.lightbrowser.Fab"
    }
    fun TAG_pref_enable_javascript():String{
        return "org.lenchan139.lightbrowser.TAG_pref_enable_javascript"
    }
    fun TAG_pref_allow_dom_storage():String{
        return "org.lenchan139.lightbrowser.TAG_pref_allow_dom_storage"
    }
    fun TAG_pref_allow_zooming():String{
        return "org.lenchan139.lightbrowser.TAG_pref_allow_zooming"
    }
    fun TAG_pref_enable_buildin_zooming():String{
        return "org.lenchan139.lightbrowser.TAG_pref_enable_buildin_zooming"
    }
    fun TAG_pref_enable_webview_cache():String{
        return "org.lenchan139.lightbrowser.TAG_pref_enable_webview_cache"
    }
    fun TAG_pref_save_password():String{
        return "org.lenchan139.lightbrowser.TAG_pref_save_password"
    }
    fun TAG_pref_image_auto_load():String{
        return "org.lenchan139.lightbrowser.TAG_pref_image_auto_load"
    }
    fun TAG_pref_finish_onStop():String{
        return "org.lenchan139.lightbrowser.TAG_pref_finish_onStop"
    }
    fun ARRAY_pref_fab(): Array<String> {
        return arrayOf(context.getString(R.string.common_string_array_pref_fab_option_disable),
                        context.getString(R.string.common_string_array_pref_fab_option_home),
                        context.getString(R.string.common_string_array_pref_fab_option_refresh),
                        context.getString(R.string.common_string_array_pref_fab_option_share),
                        context.getString(R.string.common_string_array_pref_fab_option_context_menu),
                        context.getString(R.string.common_string_array_pref_fab_option_tab_switch),
                        context.getString(R.string.common_string_array_pref_fab_option_open_with))
    }
    fun ARRAY_pref_Sharing_Format(): Array<String> {
        return arrayOf( context.getString(R.string.common_string_array_sharing_format_0),
                context.getString(R.string.common_string_array_sharing_format_1))
    }

    fun ARRAY_pref_Search_Engine_Default(): Array<SearchEngineItem> {
        return arrayOf(SearchEngineItem("DuckDuckGo", "https://duckduckgo.com/?q=!@keywoard"),
                SearchEngineItem("Google", "https://www.google.com/search?q=!@keywoard"))
    }

    fun STR_INTENT_MainToSetting(): String {
        return "org.lenchan139.lightbrowser.STR_INTENT_MainToSetting"
    }

    fun TAG_pref_oc_bookmark_url(): String {
        return "org.lenchan139.lightbrowser.TAG_pref_oc_bookmark_url"
    }
    fun TAG_pref_custom_user_agent(): String {
        return "org.lenchan139.lightbrowser.TAG_pref_custom_user_agent"
    }
    fun TAG_pref_custom_user_agent_default(): String {
        return "org.lenchan139.lightbrowser.TAG_pref_custom_user_agent_default"
    }
    fun TAG_pref_sharing_format_pref_key(): String {
        return context.getString(R.string.pref_key_share_format)
    }
    fun TAG_pref_sharing_format_string(): String {
        return "org.lenchan139.lightbrowser.TAG_pref_sharing_format_string"
    }
    fun TAG_pref_Search_Engine_Url(): String {
        return "org.lenchan139.lightbrowser.TAG_pref_Search_Engine_Url"
    }

    fun TAG_pref_Search_Engine_Name(): String {
        return "org.lenchan139.lightbrowser.TAG_pref_Search_Engine_Name"
    }

    fun URL_DDG(): String {
        return "https://duckduckgo.com"
    }
    fun TAG_custom_script_isEdit():String{
        return "TAG_custom_script_isEdit"
    }

    fun TAG_custom_script_editTitle():String{
        return "TAG_custom_script_editTitle"
    }
    fun TAG_pref_auto_open_external_when_loading():String{
        return "org.lenchan139.lightbrowser.TAG_pref_AutoOpenExternal13"
    }

    fun getInstantSuggestApiUrl(keyword: String): String{
        val query = URLEncoder.encode(keyword, "UTF-8")
        val url = String.format("https://ac.duckduckgo.com/ac/?q=%s&type=list", query)
        return url
    }
    fun getLastUrlPrefTag():String{
        return "last_url_pref_tag"
    }
    class SearchEngineItem(val name: String, val url: String)
}
