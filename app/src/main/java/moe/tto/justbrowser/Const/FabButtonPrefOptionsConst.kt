package moe.tto.justbrowser.Const

import android.content.Context
import moe.tto.justbrowser.R

class FabButtonPrefOptionsConst constructor(val context: Context){
    val DISABLE = context.getString(R.string.common_string_array_pref_fab_option_disable)
    val HOME = context.getString(R.string.common_string_array_pref_fab_option_home)
    val REFRESH = context.getString(R.string.common_string_array_pref_fab_option_refresh)
    val SHARE = context.getString(R.string.common_string_array_pref_fab_option_share)
    val CONTEXT_MENU = context.getString(R.string.common_string_array_pref_fab_option_context_menu)
    val TAB_SWITCH = context.getString(R.string.common_string_array_pref_fab_option_tab_switch)
    val OPEN_WITH = context.getString(R.string.common_string_array_pref_fab_option_open_with)
    val NC_BOOKMARK = context.getString(R.string.common_string_array_pref_fab_option_ncbookmark)
}