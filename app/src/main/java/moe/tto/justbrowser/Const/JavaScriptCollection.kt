package moe.tto.justbrowser.Const

class JavaScriptCollection {
    companion object {
        val js_isAllElementsScrollTopEqualZero =
                "function js_isAllElementsScrollTopEqualZero(){\n" +
                "    var all = document.getElementsByTagName('*');\n" +
                "    for(i of all){\n" +
                "    if(i && i.scrollTop && i.scrollTop > 0){\n" +
                "      return false;\n" +
                "        }\n" +
                "    }\n" +
                "return true;\n" +
                "}\n" +
                "js_isAllElementsScrollTopEqualZero();"
    }
}