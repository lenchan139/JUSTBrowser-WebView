package moe.tto.justbrowser.Items

import android.graphics.drawable.Drawable

class SearchListItem (var icon:Drawable?, var title:String, var url:String)