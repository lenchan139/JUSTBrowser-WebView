package moe.tto.justbrowser.Settings

import android.view.View
import android.widget.TextView

/**
 * Created by len on 27/1/2017.
 */

class SettingsViewHolder(internal var title: TextView, internal var content: TextView, internal var clickHandler: View.OnClickListener)
