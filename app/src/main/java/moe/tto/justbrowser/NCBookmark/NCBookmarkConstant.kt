package moe.tto.justbrowser.NCBookmark

import android.util.Log
import android.util.Patterns
import org.jsoup.Connection
import org.jsoup.Jsoup

class NCBookmarkConstant{
    companion object {
        val API_ENDPOINT = "/apps/bookmarks/public/rest/v2"
        val TAG_ENDPOINT = "${API_ENDPOINT}/tag"
        val BOOKMARK_ENDPOINT = "${API_ENDPOINT}/bookmark"
        val FOLDER_ENDPOINT = "${NCBookmarkConstant.API_ENDPOINT}/folder"
        fun getBookmarkEndPonint(folderId:String): String {
            var folderId = folderId
            if(folderId.isEmpty())
                folderId = "-1"
            return String.format("%s%s%s", NCBookmarkConstant.API_ENDPOINT, "/bookmark?folder=", folderId)
        }
        private val ddg_url = "https://www.duckduckgo.com"
        private val default_jsoup_timeout = 30*1000
        private val default_user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.102 Safari/537.36 Vivaldi/2.0.1309.42"
        fun getDefaultJsoup(method: Connection.Method, url:String, base64login:String): Connection {
            if(Patterns.IP_ADDRESS.matcher(url).matches()){}
            Log.v("jsoup dl url", url)
            return Jsoup.connect(url)
                .referrer(ddg_url)
                .userAgent(default_user_agent)
                .ignoreHttpErrors(true)
                .ignoreContentType(true)
                .header("Authorization", "Basic " + base64login)
                .method(method)
                .timeout(default_jsoup_timeout)
        }

    }
}