package moe.tto.justbrowser.NCBookmark

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NCBookmarkItem(
    var bookmarkId :Int,
    var title:String,
    var url:String,
    var folderId:String
): Parcelable