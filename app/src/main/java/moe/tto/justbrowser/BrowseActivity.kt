package moe.tto.justbrowser

import android.Manifest
import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.*

import android.os.Bundle
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.PersistableBundle
import android.preference.PreferenceManager
import android.text.method.ScrollingMovementMethod
import android.util.DisplayMetrics
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.webkit.CookieManager
import android.webkit.CookieSyncManager
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.viewpager.widget.ViewPager
import com.evernote.android.state.State

import kotlinx.android.synthetic.main.activity_browse.*
import kotlinx.android.synthetic.main.app_bar_browse.*
import kotlinx.android.synthetic.main.content_browse.*
import moe.tto.justbrowser.Adapter.BrowseStateFragmentPageApdapter
import moe.tto.justbrowser.Adapter.SwitchTabDialogAdapter
import moe.tto.justbrowser.Const.CommonStrings
import moe.tto.justbrowser.Const.FabButtonPrefOptionsConst
import moe.tto.justbrowser.DialogBuilder.BrowseActivity.AddressBarLongPressDialogBuilder
import org.jetbrains.anko.doAsync
import moe.tto.justbrowser.Fragment.BrowseFragment
import moe.tto.justbrowser.History.HistroySQLiteController
import moe.tto.justbrowser.NCBookmark.NCBookmarkMainController
import moe.tto.justbrowser.Tab.TabItem
import moe.tto.justbrowser.UiClass.ClearableEditText
import moe.tto.justbrowser.Utils.AdBlocker
import moe.tto.justbrowser.Utils.JavaUtils
import moe.tto.justbrowser.Utils.PreferenceUtils
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlin.collections.ArrayList

class BrowseActivity : AppCompatActivity(), SwitchTabDialogAdapter.SwitchTabDialogAdapterCallback {

    // val FILECHOOSER_RESULTCODE = 859
    // var mUploadMessage: ValueCallback<Array<Uri>>? = null
    private val MAX_FRAGMENT_NUMBERS = 99
    val BROWSE_ITEM_KEYSTRING = "BROWSE_ITEM_KEYSTRING"
    @State lateinit var browseAdapter : BrowseStateFragmentPageApdapter
    lateinit var adBlock : AdBlocker
    @State var arrBrowseFragment = ArrayList<BrowseFragment>()
    var incomeUrlOnStart : String? = null
    lateinit var settings : SharedPreferences
    lateinit var commonStrings : CommonStrings
    var tabSwitchDialog : AlertDialog? = null
    lateinit var histroySQLiteController: HistroySQLiteController
    lateinit var fabButtonPrefOptionsConst: FabButtonPrefOptionsConst
    var uniqueDialog : AlertDialog? = null
    lateinit var preferenceUtils: PreferenceUtils
    lateinit var ncbookmarkController : NCBookmarkMainController
    // val SWITCH_TAB_RESULT_CODE = 9109
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browse)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.menu_icon_ncbookmark_white_24dp)
        }
        ncbookmarkController = NCBookmarkMainController(this)
        commonStrings = CommonStrings(this)
        settings = PreferenceManager.getDefaultSharedPreferences(this)
        adBlock = AdBlocker.getInstance(this)
        fabButtonPrefOptionsConst = FabButtonPrefOptionsConst(this)
        histroySQLiteController = HistroySQLiteController(this)
        preferenceUtils = PreferenceUtils(this@BrowseActivity)
        val list = ArrayList<String>()
        list.add(BROWSE_ITEM_KEYSTRING)
        browseAdapter = BrowseStateFragmentPageApdapter(supportFragmentManager, list)
        viewPager.adapter = browseAdapter
        initViewPageOnChangeListener()
        incomeUrlOnStart = intent.getStringExtra(getString(R.string.KEY_INURL_INTENT))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
        }
        permissionChecker()
        initInUrl()
        btnSwitchTabs.setOnClickListener {
            switchTab(this)
        }
        btnSwitchTabs.setOnLongClickListener {
            //delTabDialog(this)
            true

        }
        editText.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            // If the event is a key-down event on the "enter" button
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                // Perform action on key press
                loadUrlFromEditTextToFragment()
                hideKeybord()
                return@OnKeyListener true
            }
            false
        })
        editText.setOnClickListener {
            val intent = Intent(this@BrowseActivity, SearchActivity::class.java)
            intent.putExtra("para", editText.text.toString())
            startActivity(intent)
            //arrBrowseFragment.get(viewPager.currentItem).rootView.webView.requestFocus()
        }
        editText.setOnLongClickListener {
            val dialog = AddressBarLongPressDialogBuilder.create(this@BrowseActivity, getCurrentFragment().getWebUrl())
            showUniqueDialog(dialog)
            false
        }
        editText.isFocusable = false
        decideOpenExternalButton("")

    }
    fun setDrawerEdge(enable: Boolean){
        if(!enable)
            return
        val dragger = drawer_layout.javaClass.getDeclaredField("mLeftDragger")
        dragger.isAccessible = true
        val draggerObj = dragger.get(drawer_layout)
        val edgeSize = draggerObj.javaClass.getDeclaredField("mEdgeSize")
        edgeSize.isAccessible = true
        val edge = edgeSize.getInt(draggerObj)
        Log.v("edgeIntSize0", viewPager.measuredWidth.toString())
        Log.v("edgeIntSize", edge.toString())
        edgeSize.setInt(draggerObj, edge * 10)
    }
    override fun onPostCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onPostCreate(savedInstanceState, persistentState)

    }

    fun setNavViewToFullScreen(){
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        val params = nav_view.layoutParams
        params.width = metrics.widthPixels
        nav_view.layoutParams = params
    }
    override fun onPause() {
        tabSwitchDialog?.dismiss()
        super.onPause()
    }

    override fun onBackPressed() {
        if (arrBrowseFragment.get(viewPager.currentItem).getCurrWebView().canGoBack()) {
            arrBrowseFragment.get(viewPager.currentItem).getCurrWebView().goBack()
        } else {
            exitDialog()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_browse, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        when(id){
            android.R.id.home -> drawer_layout.openDrawer(GravityCompat.START)
            R.id.action_settings -> startActivity(Intent(this@BrowseActivity, PrefActivity::class.java))
            R.id.menu_home -> menuActionLoadHomePage()
            R.id.menu_external -> menuActionRunToExternal()
            R.id.menu_history -> openHistoryActivity()
            R.id.menu_add_NCBookmarks -> openAddToNCViewer()
            R.id.menu_view_ncbookmarks -> openNCViewer()
            R.id.menu_exit -> exitDialog()
            R.id.menu_refresh -> getCurrentFragment().webView.reload()
            R.id.menu_tab -> switchTab(this@BrowseActivity)
            R.id.menu_find -> findContent()
            R.id.menu_custom_script -> openCustomScriptActivity()
            R.id.menu_desktop_mode_switch -> switchDesktopOrMobile()
            R.id.menu_jslog -> showJSLogDialog()
            R.id.menu_privacy_clean -> showPrivacyCleanDialog(this)
            R.id.menu_share -> shareCurrPage()
            else -> return false
        }
        return true
    }


    override fun onKeyLongPress(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            val tempWebiew = arrBrowseFragment.get(viewPager.currentItem).getCurrWebView()
            val webList = tempWebiew.copyBackForwardList()

            //create String[] for showing
            val items = arrayOfNulls<String>(webList.size)
            //store list to string[] with reverse sorting
            for (i in 0..webList.size - 1) {
                var temp = webList.getItemAtIndex(webList.size - 1 - i).title
                //handling if current tab
                if (i == webList.size - 1 - webList.currentIndex) {
                    //Log.v("test",String.valueOf(webList.getSize() -1 - webList.getCurrentIndex()) );
                    temp = "◆" + temp
                } else {
                    temp = "◇" + temp
                }

                if (temp.length > 50) {
                    temp = temp.substring(0, 50) + " ..."
                }
                //if title too short, use url instead
                if (temp.length > 3) {
                    items[i] = temp
                } else {
                    items[i] = temp
                }
            }

            val dialog = AlertDialog.Builder(this).setTitle(getString(R.string.string_history_colon))
                    .setItems(items) { dialog, which1 ->
                        var which = which1
                        //Toast.makeText(MainActivity.this, items[which], Toast.LENGTH_SHORT).show();
                        if (which >= 0) {
                            //reverse the number
                            which = webList.size - 1 - which
                            val pushingUrl = webList.getItemAtIndex(which).url
                            //int a1 = which - webView.copyBackForwardList().getCurrentIndex();
                            //Log.v("test", String.valueOf(a1));
                            tempWebiew.goBackOrForward(which - tempWebiew.copyBackForwardList().currentIndex)
                            //webView.loadUrl(pushingUrl);


                        }
                    }.create()
            dialog.show()

            return true
        }
        return super.onKeyLongPress(keyCode, event)
    }
    fun menuActionLoadHomePage(){
        getCurrentFragment().loadUrl(settings.getString(commonStrings.TAG_pref_home(), ""))
    }
    fun menuActionRunToExternal(){
        runToExternal(getCurrentFragment().webView.url)
    }
    fun openHistoryActivity(){
        val intent = Intent(this, HistoryActivity::class.java)
        startActivity(intent)
    }
    fun showPrivacyCleanDialog(activity: BrowseActivity){
        val arrPrivacyOptions = arrayOf(
                getString(R.string.string_cache),
                getString(R.string.string_cookies),
                getString(R.string.string_browsing_history))
                as Array<CharSequence>
        val isChoiceChecked : BooleanArray = booleanArrayOf(false,false,false)
        val dialog = AlertDialog.Builder(activity)
        val onChoice = DialogInterface.OnMultiChoiceClickListener{ dialog, which, isChecked ->
            isChoiceChecked[which] = isChecked
        }
        val onSave = DialogInterface.OnClickListener { dialog, which ->
            var msg = ""
            if(isChoiceChecked[0]){
                cleanCache()

            }
            if(isChoiceChecked[1]){
                cleanCookies()
            }
            if(isChoiceChecked[2]){
                removeAllHistory()
            }
            for(it in 0..arrPrivacyOptions.size-1){
                if(isChoiceChecked[it] && msg.length>=1){
                    msg += ", " + arrPrivacyOptions[it]
                }else{
                    msg += arrPrivacyOptions[it]
                }
            }
            msg += getString(R.string.string_has_been_cleaned)
            Toast.makeText(activity,msg,Toast.LENGTH_LONG).show()

        }
        dialog.setTitle(getString(R.string.string_select_what_do_you_want_to_clean))
                .setMultiChoiceItems(arrPrivacyOptions,isChoiceChecked,onChoice)
                .setNegativeButton("Cancel",null)
                .setPositiveButton("Clean!",onSave)
                .create().show()
    }
    override fun onPostResume() {
        super.onPostResume()
        initFabButton()
        reloadAdblockPreference()
        updateSwitchCount()
        val lastUrl =
                settings.getString(commonStrings.getLastUrlPrefTag(),
                        settings.getString(commonStrings.TAG_pref_home(),
                                commonStrings.homePage()))
        if(arrBrowseFragment.size > 0)
            checkIfEmptyTabsArray(lastUrl)
    }
    fun initNavDrawer(){

    }
    fun checkIfEmptyTabsArray(url:String?):Boolean{

        if(browseAdapter.count != arrBrowseFragment.size){
            val isRemoveInstead = settings.getBoolean(commonStrings.TAG_pref_remove_instead_restart_when_tab_mismatch(), false)
            if(isRemoveInstead)
                removeInvalidFragments(url)
            else
                restartAppProcess(url)
            return true
        }
        return false
    }
    fun removeInvalidFragments(url:String?){
        for(fragment in arrBrowseFragment){
            if(!fragment.isInit){
                arrBrowseFragment.remove(fragment)
            }
        }
        addTab()
        editText.setText(url)
        loadUrlFromEditTextToFragment()
    }
    fun switchDesktopOrMobile(){
        getCurrentFragment().webView.setDesktopMode(!getCurrentFragment().webView.getDesktopModeStatus())
        getCurrentFragment().webView.reload()
    }
    fun restartAppProcess(url:String?){
        val intent = Intent(this@BrowseActivity, UrlFromExternalActivity::class.java)
        if(url == null){
            try {
                val intentUrl = getCurrentFragment().url
                intent.setData(Uri.parse(intentUrl))
            }catch(e:Exception){
                val homeUrl = settings.getString(commonStrings.TAG_pref_home(), commonStrings.homePage())
                val lastUrl = settings.getString(commonStrings.getLastUrlPrefTag(), homeUrl)
                intent.setData(Uri.parse(lastUrl))
            }
        }else {
            //settings.edit().putString(commonStrings.getLastUrlPrefTag(), url).apply()
            intent.setData(Uri.parse(url))
        }
        val pendingIntentId = 89640
        val pendingIntent = PendingIntent.getActivity(this, pendingIntentId,    intent, PendingIntent.FLAG_CANCEL_CURRENT);

        val mgr = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        Toast.makeText(this@BrowseActivity, "Restarting " + getString(R.string.app_name), Toast.LENGTH_LONG).show()
        startActivity(intent)
        finish()
    }
    fun removeAllHistory(){
        val isRemoved = histroySQLiteController.removeAllHistory(histroySQLiteController.verifyKey)
    }
    fun cleanCache(){
        for(v in arrBrowseFragment){
            v.webView.clearCache(true)
        }
    }
    fun cleanCookies(){
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().startSync();
        CookieManager.getInstance().removeSessionCookie();
    }
    fun showUniqueDialog(dialog: AlertDialog){
        if(uniqueDialog != null && uniqueDialog!!.isShowing)
            uniqueDialog!!.dismiss()
        uniqueDialog = dialog
        if(uniqueDialog != null)
            uniqueDialog!!.show()
    }
    fun reloadAdblockPreference(){
        doAsync {
            val isAdBlockEnabled = settings.getBoolean(commonStrings.TAG_pref_enable_adblocker(),true)
            adBlock.setAdblockEnabled(isAdBlockEnabled)
            adBlock.reloadBlockedHostFile(this@BrowseActivity)
        }
    }
    fun showJSLogDialog(){
        val dialog = AlertDialog.Builder(this@BrowseActivity)

        val txtView = TextView(this@BrowseActivity)
        val jsLog = getCurrentFragment().jsConsoleLogs.joinToString("\n\n===============\n\n", "", "", -1, "", null)
        dialog.setTitle("Console Logs")
        dialog.setPositiveButton("Close", DialogInterface.OnClickListener { dialogInterface, i ->
            dialogInterface.dismiss()
        })
        txtView.setText(jsLog)
        txtView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20.0F)
        txtView.isVerticalScrollBarEnabled = true
        txtView.setTextIsSelectable(true)
        txtView.movementMethod = ScrollingMovementMethod()
        dialog.setView(txtView)
        Log.v("jsConsoleLog", jsLog)
        dialog.show()

    }
    override fun onNewIntent(intent: Intent) {
        val inUrl = intent.getStringExtra(getString(R.string.KEY_INURL_INTENT))
        val InURLFromExternal = intent.getBooleanExtra(getString(R.string.key_InURLFromExternal),false)
        if(checkIfEmptyTabsArray(inUrl)){
            return
        }
        if(InURLFromExternal){
            addTab()
            editText.setText(inUrl)
            loadUrlFromEditTextToFragment()
            intent.putExtra(getString(R.string.key_InURLFromExternal),false)
        }else if (inUrl != null) {
            editText.setText(inUrl)
            loadUrlFromEditTextToFragment()
        } else {
            super.onNewIntent(intent)
        }
    }

    override fun onStop() {
        if(settings.getBoolean(commonStrings.TAG_pref_finish_onStop(),false)){
            finish()
        }
        super.onStop()
    }


    override fun onDestroy() {
        finish()
        arrBrowseFragment.clear()
        super.onDestroy()
    }

    protected fun exitDialog() {
        val items = arrayOf(getString(R.string.string_yes), getString(R.string.string_no))
        val dialog = AlertDialog.Builder(this).setTitle(getString(R.string.string_exit_the_browser))
                .setPositiveButton(getString(R.string.string_exit)) { dialog, which ->
                    //Toast.makeText(MainActivity.this, items[which], Toast.LENGTH_SHORT).show();
                    settings.edit().remove(commonStrings.getLastUrlPrefTag()).apply()
                    finish()
                    android.os.Process.killProcess(android.os.Process.myPid())
                }.setNegativeButton("Cancel", null)
                .create()
        dialog.show()
    }
    fun openCustomScriptActivity(){
        val intent = Intent(this, CustomScriptActivity::class.java)
        startActivity(intent)
    }

    fun getCurrentFragment(): BrowseFragment {
        return arrBrowseFragment.get(viewPager.currentItem)
    }

    fun shareCurrPage() {

        val arrOptions = resources.getStringArray(R.array.common_string_array_sharing_format_array)
        val sfType = settings.getString(getString(R.string.pref_key_share_format), arrOptions[0])
        var sfSubject = ""
        var sfContent = ""
        val limit_char = preferenceUtils.share_body_max_char_limit
        if(title.length > limit_char)
            title.take(limit_char)
        val currWebView = getCurrentFragment().webView
        val title = currWebView.title
        if(sfType == arrOptions[1]){
            sfContent = currWebView.url
        }else if(sfType == arrOptions[2]){
            sfContent = String.format("%s\n%s", title, currWebView.url)

        }else{
            sfSubject = title
            sfContent = currWebView.url
        }


        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.type = "text/plain"
        if(sfSubject.isNotEmpty()) sendIntent.putExtra(Intent.EXTRA_SUBJECT, sfSubject)
        if(sfContent.isNotEmpty()) sendIntent.putExtra(Intent.EXTRA_TEXT, sfContent)
        startActivity(Intent.createChooser(sendIntent, getString(R.string.string_send_to)))
    }

    fun findContent(){
        val dialog = AlertDialog.Builder(this)
        val editText = ClearableEditText(this)
        editText.hint = getString(R.string.string_your_keyword)
        editText.setSingleLine()
        dialog.setView(editText)
        dialog.setTitle(getString(R.string.string_find_dotdotdot))

        dialog.setPositiveButton("Find", DialogInterface.OnClickListener { dialog, which ->
            if(editText.text == null){

            } else {
                getCurrentFragment().startFindContent(editText.text.toString())

            }
        })
        dialog.setNegativeButton(getString(R.string.setting_string_cancel),null)

        dialog.create().show()

    }
    fun updateEditTextFromCurrentPage(fragment:BrowseFragment, url:String?):Boolean{
        if(fragment.userVisibleHint){
            val tempUrl = url as CharSequence
            editText.setText(tempUrl.toString())
            updateLastUrl(url)
        }

        updateSwitchCount()
        return true
    }
    fun loadUrlFromEditTextToFragment() {
        arrBrowseFragment.get(viewPager.currentItem).loadUrl(editText.text.toString())


    }
    fun loadUrlToFragment(url:String){
        editText.setText(url)
        loadUrlFromEditTextToFragment()
    }
    fun hideKeybord() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }
    fun initFabButton() {
        val curr : String = settings.getString(commonStrings.TAG_pref_fab(), null) ?: ""
        Log.v("currFabPref", String.format("%s|%s",curr.contentEquals(fabButtonPrefOptionsConst.TAB_SWITCH), curr))
        if (curr == fabButtonPrefOptionsConst.HOME) {
            fab.visibility = View.VISIBLE
            fab.setImageResource(R.drawable.fab_home)
            fab.setOnClickListener {
                menuActionLoadHomePage()
            }
        } else if (curr.contentEquals( fabButtonPrefOptionsConst.REFRESH)) {
            fab.visibility = View.VISIBLE
            fab.setImageResource(R.drawable.fab_refresh)
            fab.setOnClickListener {
                getCurrentFragment().getCurrWebView().reload()
            }
        } else if (curr.contentEquals(fabButtonPrefOptionsConst.SHARE)) {
            fab.visibility = View.VISIBLE
            fab.setImageResource(R.drawable.fab_share)
            fab.setOnClickListener {
                shareCurrPage()
            }
        } else if (curr.contentEquals(fabButtonPrefOptionsConst.CONTEXT_MENU)) {
            fab.visibility = View.VISIBLE
            fab.setImageResource(R.drawable.fab_context_menu)
            fab.setOnClickListener {
                openContextMenu(this@BrowseActivity.main_content)
            }
        }else if (curr.contentEquals(fabButtonPrefOptionsConst.TAB_SWITCH)) {
            fab.visibility = View.VISIBLE
            fab.setImageResource(R.drawable.fab_tab_switch)
            fab.setOnClickListener {
                switchTab(this@BrowseActivity)
            }
        }else if (curr.contentEquals(fabButtonPrefOptionsConst.OPEN_WITH)){
            fab.visibility = View.VISIBLE
            fab.setImageResource(R.drawable.fab_open_with)
            fab.setOnClickListener {
                menuActionRunToExternal()
            }
        }else if(curr.contentEquals(fabButtonPrefOptionsConst.NC_BOOKMARK)) {
            fab.visibility = View.VISIBLE
            fab.setImageResource(R.drawable.fab_nc_bookmark)
            fab.setOnClickListener {
                openNCViewer()
            }
        }else {
            fab.visibility = View.GONE
        }

    }

    fun updateSwitchCount(){
        btnSwitchTabs.text = arrBrowseFragment.size.toString()
        Log.v("browseAdapterCount", browseAdapter.count.toString())
    }

    fun switchTab(activity:Activity):AlertDialog {
        updateSwitchCount()
        val items = Array<String>(arrBrowseFragment.size, { "" })
        val tabList = ArrayList<TabItem>()
        val view = LayoutInflater.from(this).inflate(R.layout.switch_tab_listview_layout, null)
        var listView = view.findViewById<ListView>(R.id.list_view)

        if(tabSwitchDialog != null){
            tabSwitchDialog!!.dismiss()
        }
        for (i in 0..arrBrowseFragment.size-1) {
            val fragment = arrBrowseFragment.get(i)
            val bitmap = fragment.lastScreenshot
            val bos = ByteArrayOutputStream()
            if(fragment.isInit) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos)
                val bArray = bos.toByteArray()
                Log.v("bitmap", saveToInternalStorage(bitmap))

                val obj = TabItem(fragment.url, fragment.title, bitmap)

                if (i == viewPager.currentItem) {
                    items.set(i, "▶" + obj.title + "\n" + obj.url)
                    obj.title = "▶" + obj.title
                } else {
                    items.set(i, "▷" + obj.title + "\n" + obj.url)
                    obj.title = "▷" + obj.title
                }
                tabList.add(obj)
            }
        }
        tabSwitchDialog = AlertDialog.Builder(this).setTitle("Tabs:")
                /*.setItems(items) { dialog, which ->
                    viewPager.setCurrentItem(which)
                    updateSwitchCount()
                }*/
                .setPositiveButton("New", DialogInterface.OnClickListener { dialog, which ->
                    addTab()
                }).create()
        tabSwitchDialog?.setOnCancelListener {
            tabSwitchDialog?.dismiss()
        }
        val inflater = getLayoutInflater()
        val convertView = inflater.inflate(R.layout.switch_tab_listview_layout, null) as View
        tabSwitchDialog?.setView(convertView)
        val lv = convertView.findViewById(R.id.list_view) as ListView
        val adapter = SwitchTabDialogAdapter(this, tabList)

        adapter.currentPage = viewPager.currentItem
        lv.adapter = adapter
        adapter.dialog =  tabSwitchDialog!!
        adapter.setCallback(this)
        tabSwitchDialog?.show()
        lv.setSelection(viewPager.currentItem)

        return tabSwitchDialog!!



    }

    override fun switchToTab(dialog: AlertDialog, position: Int) {
        viewPager.setCurrentItem(position,true)
        updateSwitchCount()
        dialog.dismiss()
    }

    override fun removeToTab(dialog: AlertDialog?, position: Int, titleUrl:String) {
        val alertDialog = AlertDialog.Builder(this)
                .setTitle("Confirm:")
                .setMessage("Are your sure remove " + titleUrl + "?")
                .setNegativeButton("No",null)
                .setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                    browseAdapter.remove(position)
                    arrBrowseFragment.removeAt(position)
                    updateSwitchCount()
                })
                .create()
        alertDialog.show()
        dialog?.dismiss()
    }

    fun  saveToInternalStorage(bitmapImage:Bitmap):String{
        var cw =  ContextWrapper(getApplicationContext());
         // path to /data/data/yourapp/app_data/imageDir
        var directory = applicationInfo.dataDir
        // Create imageDir
        var mypath= File(directory,"profile.jpg");

        var fos :FileOutputStream? = null
        try {
            fos = FileOutputStream(mypath);
       // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch ( e:Exception) {
              e.printStackTrace();
        } finally {
            try {
                fos?.close();
            } catch ( e: IOException) {
              e.printStackTrace();
            }
        }
        return directory
    }
    fun getFragmentsList():ArrayList<BrowseFragment>{
        val arraylist = ArrayList<BrowseFragment>()
        for(i in 0..browseAdapter.count-1){
            arraylist.add(browseAdapter.getFragment(i) as BrowseFragment)
        }
        return arraylist
    }
    fun addTab(){
        if(arrBrowseFragment.count() >= MAX_FRAGMENT_NUMBERS){
            toast(getString(R.string.string_add_tab_hit_limit_toast_message))
        }else {
            settings.edit().remove(commonStrings.getLastUrlPrefTag()).apply()
            browseAdapter.add(BROWSE_ITEM_KEYSTRING)
            updateSwitchCount()
            viewPager.setCurrentItem(arrBrowseFragment.size - 1)
        }
    }
    fun initInUrl(){
        val inUrl = intent.getStringExtra(getString(R.string.KEY_INURL_INTENT))
        intent.putExtra(getString(R.string.KEY_INURL_INTENT), "")
        if (inUrl != null && inUrl != "") {

        } else {

        }
    }
    fun initViewPageOnChangeListener(){
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }
            override fun onPageSelected(position: Int) {
                val fragment = getCurrentFragment()
                updateEditTextFromCurrentPage(fragment, arrBrowseFragment.get(viewPager.currentItem).getWebUrl())
                viewPager.setCurrentPageWebView(arrBrowseFragment.get(viewPager.currentItem).webView )
            }

        })
    }
    fun permissionChecker(){
        try {
            if (ContextCompat.checkSelfPermission(this@BrowseActivity,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this@BrowseActivity,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.
                    Toast.makeText(this, "This Appp need permission for Downloading, please allow it.", Toast.LENGTH_LONG).show()
                    val dialog = AlertDialog.Builder(this)
                    dialog.setTitle("Storage Access Required")
                            .setMessage("This Appp need internal storage permission for Download File, please allow it.")
                            .setCancelable(false)
                            .setPositiveButton("Grant", DialogInterface.OnClickListener { dialogInterface, i ->
                                val STORAGE_PERMISSION_ID = 112
                                ActivityCompat.requestPermissions(this@BrowseActivity,
                                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                                        STORAGE_PERMISSION_ID)

                            }).create().show()
                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "You are running Android 5 or lower, Skip Permission Checking.", Toast.LENGTH_SHORT).show()
        }


    }
    fun installApk(context :Context, apkFile : File){
        val intent =  Intent(Intent.ACTION_VIEW)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            val contentUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileProvider", apkFile);
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
        }else{
            intent.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        startActivity(intent);
    }
    fun decideOpenExternalButton(url:String?){
        if(url == null || url.isEmpty()){
            btnOpenExternal.visibility = View.GONE
            btnOpenExternal.setOnClickListener {}
        }else{
            btnOpenExternal.visibility = View.VISIBLE
            btnOpenExternal.setOnClickListener {
                runToExternal(url)
            }
        }
    }
    fun openNCViewer(){
        val launchIntent = packageManager.getLaunchIntentForPackage(getString(R.string.package_prefix_ncbookmark_viewer))
        try {
            startActivity(launchIntent)//null pointer check in case package name was not found
        } catch (e: NullPointerException) {
            Toast.makeText(this, getString(R.string.string_ncbookmark_not_installed), Toast.LENGTH_SHORT).show()
        }
    }
    fun openAddToNCViewer(){
        val launchIntent = Intent()
        launchIntent.setComponent(ComponentName(getString(R.string.package_prefix_ncbookmark_viewer),getString(R.string.package_ncbookmark_viwer_inaddbookmarkactivity)))
        launchIntent.putExtra("inUrl",getCurrentFragment().webView.url)
        launchIntent.putExtra("inTitle",getCurrentFragment().webView.title)
        try {
            startActivity(launchIntent)//null pointer check in case package name was not found
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            Toast.makeText(this, getString(R.string.string_ncbookmark_not_installed), Toast.LENGTH_SHORT).show()
        }
    }
    fun updateLastUrl(url:String?){
            settings.edit().putString(commonStrings.getLastUrlPrefTag(), url).apply()

    }
    fun openIntentSchemeUrl(url:String): Boolean{
        if(url.startsWith(getString(R.string.prefix_intent_scheme))){
            try {
                val intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                val existPackage = getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                if (existPackage != null) {
                    openIsOpenIntentDIalog(intent)
                } else {
                    val marketIntent = Intent(Intent.ACTION_VIEW);
                    marketIntent.setData(Uri.parse(getString(R.string.prefix_market_url_for_application) + intent.getPackage()));
                    openIsOpenIntentDIalog(intent)
                }
                return true
            }catch (e:java.lang.Exception){
              e.printStackTrace()
            }
        }
        return false
    }
    private fun runToExternal(url: String){
        val fragment = getCurrentFragment()
        runToExternal(fragment, url)
    }

    fun showKeyboard(){
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
    }
    fun hideKeyboard(){
        val imm =  getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = getCurrentFocus()
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    fun runToExternal(fragment:BrowseFragment, url: String) {
        // if intent uri, open it and igore below all code
        if (openIntentSchemeUrl(url))
            return
        // beginning
        val preIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        val targetedShareIntents = ArrayList<Intent>()
        val browserIntent = Intent.createChooser(preIntent, "Open with...")
        val resInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            packageManager.queryIntentActivities(preIntent, PackageManager.MATCH_ALL)
        } else {
            packageManager.queryIntentActivities(preIntent, PackageManager.MATCH_DEFAULT_ONLY)
        }

        for (resolveInfo in resInfo) {
            Log.v("listV", resolveInfo.activityInfo.packageName)
            val packageName = resolveInfo.activityInfo.packageName
            val targetedShareIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            targetedShareIntent.setPackage(packageName)
            if (!packageName.contains(applicationContext.packageName)) {
                targetedShareIntents.add(targetedShareIntent)
                Log.v("listVTureFalse", "True")
            }
        }
        if (targetedShareIntents.size > 1) {
            val chooserIntent = Intent.createChooser(
                    targetedShareIntents.removeAt(targetedShareIntents.size - 1), "Open with...")

            chooserIntent.putExtra(
                    Intent.EXTRA_INITIAL_INTENTS, JavaUtils().listToPracelable(targetedShareIntents))
            startActivity(chooserIntent)
        } else if (targetedShareIntents.size == 1) {
            openIsOpenIntentDIalog(targetedShareIntents.get(0))
        } else {
            Toast.makeText(this, getString(R.string.string_no_handler_here)
                    , Toast.LENGTH_SHORT).show()
        }

    }
    fun openIsOpenIntentDIalog(intent:Intent){
        val dialog = AlertDialog.Builder(this)
        val theIntent = packageManager.queryIntentActivities(intent, 0).get(0)
        dialog.setTitle("Open in " + theIntent.loadLabel(packageManager) + " ?")
                .setIcon(theIntent.loadIcon(packageManager))
                .setNegativeButton(getString(R.string.setting_string_cancel), DialogInterface.OnClickListener { dialogInterface, i ->

                })
                .setPositiveButton(getString(R.string.string_go), DialogInterface.OnClickListener { dialogInterface, i ->
                    startActivity(intent)
                }).create().show()
    }
}
