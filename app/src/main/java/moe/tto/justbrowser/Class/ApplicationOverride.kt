package moe.tto.justbrowser.Class

import android.app.Application
import com.evernote.android.state.StateSaver

class ApplicationOverride : Application() {
    override fun onCreate() {
        super.onCreate()
        StateSaver.setEnabledForAllActivitiesAndSupportFragments(this, true);
    }
}