package moe.tto.justbrowser.Class;


import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import moe.tto.justbrowser.WebViewClass.WebViewOverride;

import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import moe.tto.justbrowser.WebViewClass.WebViewOverride;

public class ViewPagerOverride extends ViewPager {

    private WebViewOverride mCurrentPageWebView_; //custom webview

    public ViewPagerOverride(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
            /*
            Log.v("viewPagerOverride", "CustomViewPager - onInterceptTouchEvent");


        // if view zoomed out (view starts at 33.12... scale level) ... allow
        // zoom within webview, otherwise disallow (allow viewpager to change
        // view)
        if (event.getEdgeFlags() != MotionEvent.EDGE_LEFT && mCurrentPageWebView_ != null && (mCurrentPageWebView_.getScale() * 100) > 34) {
            Log.v("viewPagerOverride", "CustomViewPager - intrcepted: " + String.valueOf((mCurrentPageWebView_.getScale() * 100)));
            this.requestDisallowInterceptTouchEvent(true);
        }
        else {
            if (mCurrentPageWebView_ != null) {
                Log.v("viewPagerOverride",
                        "CustomViewPager - not intrcepted: " + String.valueOf(mCurrentPageWebView_.getScale() * 100));
            }
            this.requestDisallowInterceptTouchEvent(false);
        }
*/
        return super.onInterceptTouchEvent(event);
    }

    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
            return true;

    }

    public WebViewOverride getCurrentPageWebView() {
        return mCurrentPageWebView_;
    }

    public void setCurrentPageWebView(WebViewOverride currentPageWebView) {
        mCurrentPageWebView_ = currentPageWebView;
    }
}