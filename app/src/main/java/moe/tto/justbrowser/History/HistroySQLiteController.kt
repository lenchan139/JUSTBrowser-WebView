package moe.tto.justbrowser.History

import android.content.Context
import android.database.Cursor
import android.util.Log
import org.jetbrains.anko.doAsync

import moe.tto.justbrowser.CustomScript.CustomScriptItem

import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Calendar
import java.util.Locale

/**
 * Created by len on 14/4/2017.
 */

class HistroySQLiteController(private val context: Context) {
    private var historySQLiteHelper: HistorySQLiteHelper? = null

    val verifyKey: String
        get() = VERIFY_KEY
    val history: ArrayList<CustomScriptItem>
        get() {
            val result = ArrayList<CustomScriptItem>()
            val db = historySQLiteHelper!!.readableDatabase
            val sqlSelct = "SELECT * FROM " + historySQLiteHelper!!.get_TableName() + " ORDER BY _id DESC"
            var cursor: Cursor? = null
            cursor = db.rawQuery(sqlSelct, null)
            Log.v("ss", cursor!!.count.toString())
            if (cursor.moveToFirst()) {
                do {
                    result.add(CustomScriptItem(cursor.getString(1), cursor.getString(2), cursor.getString(3)))
                    Log.v("status", CustomScriptItem(cursor.getString(1), cursor.getString(2), cursor.getString(3)).toString())
                } while (cursor.moveToNext())
            }
            cursor.close()
            return result
        }

    init {
        historySQLiteHelper = HistorySQLiteHelper(context)

    }

    fun removeAllHistory(verify_key: String): Boolean {
        val db = historySQLiteHelper!!.writableDatabase
        if (verify_key == VERIFY_KEY) {
            val sqlDrop = "DROP TABLE " + historySQLiteHelper!!.get_TableName()
            db.execSQL(sqlDrop)
            db.execSQL(historySQLiteHelper!!.sqlCreate)
            return true
        } else {
            return false
        }
    }

    fun addHistory(title: String, url: String) {
        val db = historySQLiteHelper!!.writableDatabase
        /*ContentValues values = new ContentValues();
        values.put("_title", title);
        values.put("_url", url);
        values.put("_addDate", new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime()));
        db.insert(historySQLiteHelper.get_TableName(), null, values);
        */
        doAsync {
            if (title.length > 0 && url.length > 0) {
                val sqlInsert = "INSERT INTO " + historySQLiteHelper!!.get_TableName() +
                        "(_title,_url,_addDate) VALUES(\"" +
                        title + "\",\"" + url + "\",\"" +
                        SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(Calendar.getInstance().time) + "\")"
                Log.v("logHistroyAdd", sqlInsert)
                db.execSQL(sqlInsert)
            }
            db.close()
        }

    }

    fun getHistoryBySearchUrl(keyword: String?): ArrayList<CustomScriptItem> {
        keyword!!.replace(" ", "%")
        val result = ArrayList<CustomScriptItem>()
        val db = historySQLiteHelper!!.readableDatabase
        var sqlSelct = "SELECT * FROM " + historySQLiteHelper!!.get_TableName() +
                " WHERE _url LIKE \"%" + keyword + "%\"" +
                " OR _title LIKE \"%" + keyword + "%\"" +
                " GROUP BY _title,_url" +
                " ORDER BY LENGTH(_url) + LENGTH(_title)*2 ASC"
        var cursor: Cursor? = null
        if (keyword == null || keyword == "") {
            sqlSelct = "SELECT * FROM " + historySQLiteHelper!!.get_TableName() + "WHERE 2=1"
        }
        cursor = db.rawQuery(sqlSelct, null)
        Log.v("ss", cursor!!.count.toString())
        if (cursor.moveToFirst()) {
            do {
                result.add(CustomScriptItem(cursor.getString(1), cursor.getString(2), cursor.getString(3)))
                Log.v("status", CustomScriptItem(cursor.getString(1), cursor.getString(2), cursor.getString(3)).toString())
            } while (cursor.moveToNext())
        }
        cursor.close()
        return result
    }

    companion object {
        private val VERIFY_KEY = "sajidajoidajsfioajsfiovndb jduigrefjdksbv jueir4839fhie"
    }
}
