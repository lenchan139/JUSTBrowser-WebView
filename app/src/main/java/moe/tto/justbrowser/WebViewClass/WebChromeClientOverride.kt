package moe.tto.justbrowser.WebViewClass

import android.R
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Message
import android.util.Log
import android.view.View
import android.webkit.ConsoleMessage
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.fragment_browse.view.*
import moe.tto.justbrowser.BrowseActivity
import moe.tto.justbrowser.Fragment.BrowseFragment
import android.view.ViewGroup



class WebChromeClientOverride(val activity: BrowseActivity, val fragment: BrowseFragment) : WebChromeClient() {
    private var customViewCallback: WebChromeClient.CustomViewCallback? = null
    private var customView: View? = null
    private var customViewDialog : AlertDialog? = null
    override fun onCloseWindow(window: WebView) {
        activity.onBackPressed()
        super.onCloseWindow(window)
    }

    override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
        if(customViewCallback != null){
            customViewCallback!!.onCustomViewHidden()
            customViewCallback = null;
            return

        }
        showFullScreenDialog(view, callback)
    }

    override fun onHideCustomView() {
        activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_USER
        if(customView != null){
            if(customViewCallback != null){
                customViewCallback?.onCustomViewHidden()
                customViewCallback = null
            }
            customViewDialog?.dismiss()
            customViewDialog = null
        }
    }

    fun showFullScreenDialog(view:View?, callback: CustomViewCallback?){
        customViewDialog = AlertDialog
                .Builder(activity, R.style.Theme_Black_NoTitleBar_Fullscreen)
                .setView(view)
                .create()
        customView = view
        customViewCallback = callback
        if(view != null){
            val width = view.width
            val height = view.height
            if(width >= height){
                activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
            }else{
                activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT
            }
        }
        //activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        customViewDialog?.show()

    }
    override fun onConsoleMessage(consoleMessage: ConsoleMessage?): Boolean {
        Log.v("onAddConsoleMsg",consoleMessage.toString())
        if(consoleMessage != null) {
            val line = String.format("%s \n Line %s:\n %s", consoleMessage.sourceId(),  consoleMessage.lineNumber(), consoleMessage.message())
            fragment.jsConsoleLogs.add(line)
        }
        return super.onConsoleMessage(consoleMessage)
    }

    override fun onCreateWindow(view: WebView?, isDialog: Boolean, isUserGesture: Boolean, resultMsg: Message?): Boolean {
        //activity.addTab()
        return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg)
    }

    override fun onReceivedTitle(view: WebView, title: String) {
        super.onReceivedTitle(view, title)
        Log.v("currWebViewTitle", title)
    }

    override fun onShowFileChooser(webView: WebView?, filePathCallback: ValueCallback<Array<Uri>>?, fileChooserParams: FileChooserParams?): Boolean {
        if (fragment.mUploadMessage != null) {
            fragment.mUploadMessage!!.onReceiveValue(null)
        }
        Log.i("UPFILE", "file chooser params：" + fileChooserParams!!.toString())
        fragment.mUploadMessage = filePathCallback
        val i = Intent(Intent.ACTION_GET_CONTENT)
        i.addCategory(Intent.CATEGORY_OPENABLE)

        Log.v("acceptType", fileChooserParams.acceptTypes[0].toString())
        if (fileChooserParams.acceptTypes != null && fileChooserParams.acceptTypes.size > 0) {
            i.type = fileChooserParams.acceptTypes[0]
            i.type = "*/*"
        } else {
            i.type = "*/*"
        }
        fragment.startActivityForResult(Intent.createChooser(i, "File Chooser"), fragment.FILECHOOSER_RESULTCODE)

        return true
    }

    override fun onProgressChanged(view: WebView, progress: Int) {
        Log.v("webview " + view.id , "[onProgressChanged triggered]:["+progress.toString()+"]")
        if (progress < 100) {
            fragment.rootView.progressL.visibility = ProgressBar.VISIBLE
            fragment.rootView.progressL.progress = progress
        } else if (progress >= 100) {
            fragment.rootView.progressL.progress = progress

            try {
                Thread.sleep(300)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            fragment.updateScreenshot()
            //progLoading.setVisibility(ProgressBar.INVISIBLE);
            fragment.rootView.progressL.progress = 0
            fragment.rootView.progressL.visibility = ProgressBar.GONE
        }

    }
}