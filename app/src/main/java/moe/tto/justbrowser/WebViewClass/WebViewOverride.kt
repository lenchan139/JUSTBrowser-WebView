package moe.tto.justbrowser.WebViewClass

import android.app.DownloadManager
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.util.AttributeSet
import android.util.Log
import android.view.ContextMenu
import android.view.MenuItem
import android.view.MotionEvent
import android.webkit.WebView
import android.widget.Toast
import icepick.Icicle
import moe.tto.justbrowser.Const.JavaScriptCollection
import moe.tto.justbrowser.Const.WebElementLongClickMenuOptions
import moe.tto.justbrowser.R


/**
 * Created by len on 12/16/16.
 */

class WebViewOverride : WebView {
    @Icicle private var isDesktopMode = false
    @Icicle internal var context: Context
    val longClickOptions = WebElementLongClickMenuOptions()
    private var m_isAllElementsOnTop = false
    val isAllElementsOnTop get() = m_isAllElementsOnTop
    constructor(context: Context) : super(context) {
        this.context = context
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        this.context = context
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, privateBrowsing: Boolean) : super(context, attrs, defStyleAttr, privateBrowsing) {
        this.context = context
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        this.context = context
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.context = context
    }


    override fun onCreateContextMenu(menu: ContextMenu) {
        super.onCreateContextMenu(menu)

        val result = hitTestResult

        val handler = MenuItem.OnMenuItemClickListener { item ->
            val id = item.itemId

            val url = result.extra as String

            if (id == longClickOptions.saveImage) {
                var request = DownloadManager.Request(
                        Uri.parse(url))

                request.allowScanningByMediaScanner()
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED) //Notify client once download is completed!
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, url.substring(url.lastIndexOf("/")))
                val dm = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

                dm.enqueue(request)

                val intent = Intent(Intent.ACTION_OPEN_DOCUMENT) //This is important!
                intent.addCategory(Intent.CATEGORY_OPENABLE) //CATEGORY.OPENABLE
                intent.type = "*/*"//any application,any extension

                Toast.makeText(context, context.getString(R.string.string_webview_string_start_downloading), Toast.LENGTH_SHORT).show()

            } else if (id == longClickOptions.viewImage) {
                loadUrl(url)
                Log.v("menuId", context.getString(R.string.webview_string_view_image))
            } else if (id == longClickOptions.copyUrl) {
                val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip = ClipData.newPlainText("cilps", url)
                clipboard.primaryClip = clip
                Toast.makeText(context, context.getString(R.string.webview_string_url_copied), Toast.LENGTH_SHORT).show()
                Log.v("menuId", context.getString(R.string.webview_string_copy_url))
            } else if (id == longClickOptions.shareUrl) {

                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.putExtra(Intent.EXTRA_TEXT, url)
                sendIntent.type = "text/plain"
                context.startActivity(Intent.createChooser(sendIntent, context.getString(R.string.string_send_to)))
                Log.v("menuId", "Share Link")
            }
            true
        }

        if (result.type == WebView.HitTestResult.IMAGE_TYPE || result.type == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE) {
            // Menu options for an image.
            //set the header title to the image url
            menu.setHeaderTitle(result.extra)
            menu.add(0, longClickOptions.saveImage, 0, context.getString(R.string.webview_string_save_image)).setOnMenuItemClickListener(handler)
            menu.add(0, longClickOptions.viewImage, 0, context.getString(R.string.webview_string_view_image)).setOnMenuItemClickListener(handler)
            menu.add(0, longClickOptions.copyUrl, 0, context.getString(R.string.webview_string_copy_url)).setOnMenuItemClickListener(handler)
            menu.add(0, longClickOptions.shareUrl, 0, context.getString(R.string.webview_string_share_url)).setOnMenuItemClickListener(handler)
        }else if (result.type == WebView.HitTestResult.ANCHOR_TYPE || result.type == WebView.HitTestResult.SRC_ANCHOR_TYPE) {
            // Menu options for a hyperlink.
            //set the header title to the link url
            menu.setHeaderTitle(result.extra)
            menu.add(0, longClickOptions.copyUrl, 0, context.getString(R.string.webview_string_copy_url)).setOnMenuItemClickListener(handler)
            menu.add(0, longClickOptions.shareUrl, 0, context.getString(R.string.webview_string_share_url)).setOnMenuItemClickListener(handler)
        }else{
            Log.v("clieckedElementType", result.type.toString())
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        this.evaluateJavascript(JavaScriptCollection.js_isAllElementsScrollTopEqualZero, { value ->
            m_isAllElementsOnTop = value.toBoolean()
            Log.v("ScrollTopEqualZero", m_isAllElementsOnTop.toString())
        })
        return super.onTouchEvent(event)
    }

    fun setDesktopMode(enabled: Boolean) {
        val webSettings = settings

        val newUserAgent: String
        if (enabled) {
            newUserAgent = webSettings.userAgentString.replace("Mobile", "eliboM").replace("Android", "diordnA")
            isDesktopMode = true
        } else {
            newUserAgent = webSettings.userAgentString.replace("eliboM", "Mobile").replace("diordnA", "Android")
            isDesktopMode = false
        }

        webSettings.userAgentString = newUserAgent
        webSettings.useWideViewPort = enabled
        webSettings.loadWithOverviewMode = enabled
        webSettings.setSupportZoom(enabled)
        webSettings.builtInZoomControls = enabled
    }
    fun getDesktopModeStatus() : Boolean{
        return isDesktopMode
    }
    override fun performClick(): Boolean {
        return true
    }
    fun canScrollHor(direction: Int): Boolean {
        val offset = computeHorizontalScrollOffset()
        val range = computeHorizontalScrollRange() - computeHorizontalScrollExtent()
        if (range == 0) return false
        return if (direction < 0) {
            offset > 0
        } else {
            offset < range - 1
        }
    }
}
