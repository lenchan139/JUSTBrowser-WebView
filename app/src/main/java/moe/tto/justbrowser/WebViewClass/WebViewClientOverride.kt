package moe.tto.justbrowser.WebViewClass

import android.graphics.Bitmap
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.webkit.*
import kotlinx.android.synthetic.main.content_browse.*
import kotlinx.android.synthetic.main.fragment_browse.view.*
import moe.tto.justbrowser.BrowseActivity
import moe.tto.justbrowser.Const.JavaScriptCollection
import moe.tto.justbrowser.CustomScript.CustomScriptUtil
import moe.tto.justbrowser.Fragment.BrowseFragment
import moe.tto.justbrowser.History.HistroySQLiteController
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class WebViewClientOverride(val webview: WebViewOverride, val activity: BrowseActivity, val fragment:BrowseFragment) : WebViewClient() {
    var loadingFinish: Boolean? = true
    var redirectPage: Boolean? = false
    private var back = false
    private var isUserClickUrl = false
    private var currentUrl = ""
    init {
        webview.setOnTouchListener { view, motionEvent ->
            if(view.id == webview.id && motionEvent.action == MotionEvent.ACTION_DOWN){
                isUserClickUrl = true
                currentUrl = webview.url
            }
            false
        }
    }
    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        fragment.jsConsoleLogs.clear()
        if(url != null && view != null) {
            fragment.url = url
            fragment.title = view.title
        }
        loadingFinish = false
        super.onPageStarted(view, url, favicon)
        fragment.webView.requestFocus()
        Log.v("onPageLoadUrl",url)
        if (isUrlVaildRedirect(url!!)) {
            //addToBack(url);
            activity.updateEditTextFromCurrentPage(fragment, url)
            fragment.rootView.refresh_layout.isRefreshing = true
        } else {
            back = true
            view!!.stopLoading()
            activity.updateEditTextFromCurrentPage(fragment, view.url)
        }
        tryOpenExternal(url)
        fragment.updateUrlUseJavascript(view)

    }



    var isInitDone = false
    override fun onPageFinished(view: WebView, url: String){
        isUserClickUrl = false
        currentUrl = url
        fragment.url = url
        fragment.title = view.title
        if (!redirectPage!!) {
            loadingFinish = true

        }

        if (loadingFinish!! && (!redirectPage!!)) {
            //HIDE LOADING IT HAS FINISHED
            //addToBack(url,view.getTitle());
            val hs = HistroySQLiteController(activity)
            if(view.title.isNotEmpty() && view.title.isNotEmpty()) {
                hs.addHistory(view.title, view.url)

            }
            //runCustomScript(s)
            val runscripts = CustomScriptUtil().getScriptsToRun(activity,url)
            if(runscripts.size > 0 && isInitDone){
                for(i in runscripts){
                    view.evaluateJavascript(i,null)
                }
            }
            checkAllElementsScrollTop()
            if(fragment.webView?.id == activity.viewPager.currentPageWebView?.id)
                activity.updateEditTextFromCurrentPage(fragment, url)
            tryOpenExternal(url)
            isInitDone = true
        } else {
            redirectPage = false
        }

        fragment.rootView.refresh_layout.isRefreshing = false
        fragment.updateScreenshot()
        fragment.updateUrlUseJavascript(view)
        super.onPageFinished(view, url)
    }



    override fun shouldInterceptRequest(view: WebView?, request: WebResourceRequest?): WebResourceResponse? {
        Log.v("idAdBlocked", request!!.url.toString() + " | " + activity.adBlock.isAd(request.url.toString()).toString())
        if(activity.adBlock.isAd(request.url.toString())){
            val wr =  WebResourceResponse("", "", null)
            return wr
        }
        return super.shouldInterceptRequest(view, request.url.toString())
    }

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        Log.v("loadingUrl",request!!.url.toString())
        val isVaildRedirect = isUrlVaildRedirect(request.url.toString())
        activity.updateEditTextFromCurrentPage(fragment, request.url.toString())
        if (view != null) {
            fragment.updateUrlUseJavascript(view)
        }
        val url = request.url.toString()

        if(isUserClickUrl && !url.equals(currentUrl) && fragment.isOpenExternal(url)){
            activity.runToExternal(fragment, url)
        }
        return !isVaildRedirect
    }

    override fun doUpdateVisitedHistory(view: WebView?, url: String?, isReload: Boolean) {
        fragment.updateUrlUseJavascript(view)
        super.doUpdateVisitedHistory(view, url, isReload)
    }

    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        Log.v("loadingUrlOld", url.toString())
        if ((!loadingFinish!!)) {
            redirectPage = true
        }
        loadingFinish = false
        activity.updateEditTextFromCurrentPage(fragment, url)
        view.loadUrl(url)
        //addToBack(url);
        fragment.updateUrlUseJavascript(view)
        if(url.isNotEmpty() && fragment.isInit){
            fragment.tryOpenExternal(url)
        }
        return true
    }

    override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
        super.onReceivedError(view, errorCode, description, failingUrl)
        //activity.updateEditTextFromCurrentPage(fragment, failingUrl)
    }

    override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
        super.onReceivedError(view, request, error)
        //activity.updateEditTextFromCurrentPage(fragment, request?.url.toString())
    }
    private fun tryOpenExternal(url:String){
        if(url.isNotEmpty() && fragment.isInit ){
            fragment.tryOpenExternal(url)
        }
    }
    fun checkAllElementsScrollTop(){

    }
    fun isUrlVaildRedirect(url: String):Boolean{
        if (url.startsWith("http:") || url.startsWith("https:") || url.startsWith("javascript:")) {
            return true
        } else {
            return false
        }
    }
}
