# Deprecated Repo
JUST Browser is going to rewrite on GeckoView.    
Visit: [https://gitlab.com/lenchan139/JUSTBrowser](https://gitlab.com/lenchan139/JUSTBrowser)
# JUST Browser
This is a Veeeeeeeeerrrrrry lightweight browser using webview. I made it because my needs.  
Rewrite from Light Browser with Fragment to reduce resource usage.

## Licenses
Basically, JUSTBrowser is under GPLv3 but a few file is under their lincense.

#### - Apache License 2.0
  A few files / binaries is licensing under Apache License 2.0.  
  You can go to (http://www.apache.org/licenses/LICENSE-2.0) view the full content of the license. 
  - Droidparts && ClearableEditText  
     Copyright 2016 Alex Yanchenko  
  - ArrayPagerAdapter    
     Takaaki Nakama
     
#### - GNU General Public License v2
  A few files is coding by myself. Those files is licensing under GPLv2.  
  You can go to (https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) view the full content of the license.
  
## Screenshots
 <img src="https://gitlab.com/lenchan139/JUSTBrowser/raw/master/screenshots/photo_2017-04-15_16-52-36.jpg" width="280"> <img src="https://gitlab.com/lenchan139/JUSTBrowser/raw/master/screenshots/photo_2017-04-15_16-52-32.jpg" width="280">
## Download
You can download this application from Play Store or Aptoide. Aptoide may faster than the Play Store.
- [Play Store](https://play.google.com/store/apps/details?id=moe.tto.justbrowser)
- [APK file](https://gitlab.com/lenchan139/JUSTBrowser/tags)

## Feedback
I doubt that who will use this application...  
However, you can send feedback or feature request by mailing to mail@lenchan139.org
